exports.get = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    return reply.view('admin/show');
  }
};

exports.createZip = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    this.api.call({
      method: 'POST',
      path: '/zips',
      data: request.payload
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'Unable to create new zip code. Try again.');
        return reply.redirect('/admin');
      }

      request.session.flash('info', 'Zip created');
      return reply.redirect('/admin/zips');
    });
  }
};

exports.zips = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    this.api.call({
      path: '/zips'
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'Unable to get zip codes. Try again.');
        return reply.redirect('/admin');
      }

      var context = { zips: res.body };
      return reply.view('admin/zips/index', context);
    });
  }
};

exports.showZip = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    this.api.call({
      path: '/zips/' + request.params.id
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'Unable to get zip code. Try again.');
        return reply.redirect('/admin/zips');
      }

      var context = { zip: res.body };
      return reply.view('admin/zips/edit', context);
    });
  }
};

exports.updateZip = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    if (request.payload._method === 'delete') {
      return this.api.call({
        method: 'DELETE',
        path: '/zips/' + request.params.id
      }, function(err, res) {
        if (err) {
          request.session.flash('danger', 'Unable to delete zip code. Try again.');
          return reply.redirect('/admin/zips/' + request.params.id);
        }

        request.session.flash('info', 'Zip code deleted.');
        return reply.redirect('/admin/zips');
      });
    }

    this.api.call({
      method: 'PUT',
      path: '/zips/' + request.params.id
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'Unable to update zip code. Try again.');
        return reply.redirect('/admin/zips/' + request.params.id);
      }

      request.session.flash('info', 'Zip code updated.');
      return reply.redirect('/admin/zips/' + request.params.id);
    });
  }
};

exports.invites = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    this.api.call({
      path: '/invites'
    }, function(err, res) {
      if (err) {
        request.log('error', err);
        return reply(err);
      }

      var context = { invites: res.body };
      return reply.view('admin/invites/index', context);
    });
  }
};

exports.createInvite = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    this.api.call({
      method: 'POST',
      path: '/invites'
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'Unable to create new invites code. Try again.');
        return reply.redirect('/admin');
      }

      request.session.flash('info', 'Invite created');
      return reply.redirect('/admin/invites');
    });
  }
};

exports.showInvite = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    this.api.call({
      path: '/invites/' + request.params.id
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'Unable to get invite. Try again.');
        return reply.redirect('/admin/invites');
      }

      var context = { invite: res.body };
      return reply.view('admin/invites/edit', context);
    });
  }
};

exports.updateInvite = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    if (request.payload._method === 'delete') {
      return this.api.call({
        method: 'DELETE',
        path: '/invites/' + request.params.id,
      }, function(err, res) {
        if (err) {
          request.session.flash('danger', 'Unable to delete invite. Try again.');
          return reply.redirect('/admin/invites/' + request.params.id);
        }

        request.session.flash('info', 'Invite deleted.');
        return reply.redirect('/admin/invites');
      });
    }

    this.api.call({
      method: 'PUT',
      path: '/invites/' + request.params.id,
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'Unable to update invite. Try again.');
        return reply.redirect('/admin/invites/' + request.params.id);
      }

      request.session.flash('info', 'Invite updated.');
      return reply.redirect('/admin/invites/' + request.params.id);
    });
  }
};
