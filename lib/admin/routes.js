var Admin = require('./handlers/admin');

exports.endpoints = [
  { method: 'GET', path: '/admin', config: Admin.get },
  { method: 'GET', path: '/admin/zips', config: Admin.zips },
  { method: 'POST', path: '/admin/zips', config: Admin.createZip },
  { method: 'GET', path: '/admin/zips/{id}', config: Admin.showZip },
  { method: 'POST', path: '/admin/zips/{id}', config: Admin.updateZip },
  { method: 'GET', path: '/admin/invites', config: Admin.invites },
  { method: 'POST', path: '/admin/invites', config: Admin.createInvite },
  { method: 'GET', path: '/admin/invites/{id}', config: Admin.showInvite },
  { method: 'POST', path: '/admin/invites/{id}', config: Admin.updateInvite }
];
