var Joi = require('joi');
var Boom = require('boom');
var Hoek = require('hoek');
var Invite = require('../models/invite');

exports.index = {
  auth: { mode: 'required', scope: 'admin' },
  validate: {
    query: {
      code: Joi.string(),
      name: Joi.boolean()
    }
  },
  handler: function(request, reply) {
    return Invite.find(request.query).lean().exec(function(err, invites) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(invites);
    });
  }
};

exports.create = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    var invite = new Invite;

    invite.save(function(err, res) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badData());
      }

      return reply(invite.toObject());
    });
  }
};

exports.find = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    Invite.findById(request.params.id).lean().exec(function(err, invite) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(invite);
    });
  }
};

exports.update = {
  auth: { mode: 'required', scope: 'admin' },
  validate: {
    payload: {
      code: Joi.string(),
      active: Joi.boolean()
    }
  },
  handler: function(request, reply) {
    Invite.findById(request.params.id, function(err, invite) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      if (!invite) {
        return reply(Boom.notFound());
      }

      invite = Hoek.merge(invite, request.payload);

      return invite.save(function(err, invite) {
        if (err) {
          request.log('error', err);
          return reply(Boom.badData());
        }

        return reply(invite.toObject());
      });
    });
  }
};

exports.destroy = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    Invite.findByIdAndRemove(request.params.id, function(err) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply({ status: 'ok' });
    });
  }
};
