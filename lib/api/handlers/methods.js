exports.checkInvite = function(request, reply) {
  if (request.payload.scope !== 'business') {
    return reply(false);
  }

  if (!request.payload.invite) {
    return reply(Boom.unauthorized().takeover());
  }

  Invite.findOne({ code: request.payload.invite }, function(err, invite) {
    if (err) {
      request.log('error', err);
      return reply(Boom.unauthorized().takeover());
    }

    if (!invite || invite.active) {
      return cb(Boom.unauthorized().takeover());
    }
    
    return reply(invite);
  });
};
