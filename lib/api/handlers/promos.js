var Joi = require('joi');
var Boom = require('boom');
var Async = require('async');
var Moment = require('moment');
var Hoek = require('hoek');
var Promo = require('../models/promo');

exports.index = {
  auth: false,
  validate: {
    query: {
      zips: Joi.string().length(5),
      username: Joi.string()
    }
  },
  handler: function(request, reply) {
    Promo.find({
      $or: [ { zips: request.query.zips }, { 'instagram.user.username': request.query.username } ]
    })
    .limit(10)
    .lean()
    .exec(function(err, promos) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(promos);
    });
  }
};

exports.active = {
  auth: false,
  validate: {
    query: {
      zips: Joi.string().length(5)
    }
  },
  handler: function(request, reply) {
    Promo.find({ zips: request.query.zips })
      .where('expiresAt').gt(Date.now())
      .limit(10)
      .lean()
      .exec(function(err, promos) {
        if (err) {
          request.log('error', err);
          return reply(Boom.badImplementation());
        }

        return reply(promos);
      });
  }
};

exports.find = {
  auth: false,
  handler: function(request, reply) {
    Promo.findById(request.params.id, function(err, promo) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(promo.toObject());
    });
  }
};

exports.create = {
  auth: { mode: 'required', scope: 'business' },
  validate: {
    payload: {
      creator: Joi.string().required(),
      duration: Joi.number().allow(1, 3, 6, 12, 24).required(),
      zips: Joi.array().items(Joi.string().length(5)).required(),
      text: Joi.string().allow(''),
      instagram: Joi.object({
        images: Joi.object().required(),
        user: Joi.object().required(),
        id: Joi.string().required(),
        link: Joi.string().required(),
        caption: Joi.string().allow('')
      })
    }
  },
  handler: function(request, reply) {
    var promo = new Promo(request.payload);

    promo.save(function(err, promo) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badData());
      }

      return reply(promo.toObject());
    });
  }
};

exports.update = {
  auth: { mode: 'required', scope: 'business' },
  validate: {
    payload: {
      duration: Joi.number().allow(1, 3, 6, 12, 24),
      zips: Joi.array().items(Joi.string().length(5)),
      text: Joi.string(),
      instagram: Joi.object({
        images: Joi.object(),
        user: Joi.object(),
        id: Joi.string(),
        link: Joi.string()
      })
    }
  },
  handler: function(request, reply) {
    Promo.findById(request.params.id, function(err, promo) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      if (!promo) {
        return reply(Boom.notFound());
      }

      promo = request.payload;

      promo.save(function(err, promo) {
        if (err) {
          request.log('error', err);
          return reply(Boom.badData());
        }

        return reply(promo.toObject());
      });
    });
  }
};

exports.destroy = {
  auth: { mode: 'required', scope: ['business', 'admin'] },
  handler: function(request, reply) {
    Promo.findByIdAndRemove(request.params.id, function(err) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply({ status: 'ok' });
    });
  }
};
