var Boom = require('boom');
var Joi = require('joi');
var User = require('../models/user');
var Session = require('../models/session');

exports.create = {
  auth: false,
  validate: {
    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required()
    }
  },
  handler: function(request, reply) {
    var self = this;

    User.findOne({ email: request.payload.email }, function(err, user) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      if (!user) {
        return reply(Boom.unauthorized());
      }

      user.authenticate(request.payload.password, function(err, valid) {
        if (err) {
          request.log('error', err);
          return reply(Boom.badImplementation());
        }

        if (!valid) {
          return reply(Boom.unauthorized());
        }

        var session = new Session({ user: user._id });

        session.save(function(err, sess) {
          if (err) {
            request.log('error', err);
            return reply(Boom.badImplementation());
          }

          return reply({
            token: sess._id,
            user: user.toObject(),
            scope: user.scope
          });
        });
      });
    });
  }
};

exports.find = {
  handler: function(request, reply) {
    Session.findById(request.params.id).lean().exec(function(err, session) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(session);
    });
  }
};

exports.destroy = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    var token = request.headers.authorization.split(' ')[1];

    Session.findByIdAndRemove(token, function(err) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply({ status: 'ok' });
    });
  }
};
