var Boom = require('boom');
var Joi = require('joi');
var Async = require('async');
var Hoek = require('hoek');

var User = require('../models/user');
var Session = require('../models/session');
var Invite = require('../models/invite');
var Methods = require('./methods');

exports.index = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    User.find(request.query).lean().exec(function(err, users) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(users);
    });
  }
};

exports.find = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    User.findById(request.params.id).lean().exec(function(err, user) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(user);
    });
  }
};

exports.create = {
  auth: false,
  pre: [
    { method: Methods.checkInvite, assign: 'invite' }
  ],
  validate: {
    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
      scope: Joi.string().allow('user', 'business').not('admin').required(),
      invite: Joi.string()
    }
  },
  handler: function(request, reply) {
    var self = this;
    var user = new User({
      email: request.payload.email,
      scope: request.payload.scope,
      password: request.payload.password
    });

    user.save(function(err, user) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      if (request.pre.invite) {
        invite.active = true;
      }

      var session = new Session({ user: user._id });
      var calls = [];

      calls.push(function(cb) {
        return session.save(cb);
      });

      if (request.pre.invite) {
        calls.push(function(cb) {
          return invite.save(cb);
        });
      }

      Async.parallel(calls, function(err, res) {
        if (err) {
          request.log('error', err);
          return reply(err);
        }

        var sess = res[0][0];
        return reply({
          token: sess._id,
          user: user.toObject(),
          scope: user.scope
        });
      });
    });
  }
};

exports.update = {
  auth: { mode: 'required' },
  validate: {
    payload: {
      email: Joi.string(),
      saves: Joi.array(),
      flags: Joi.array(),
      instagram: Joi.object({
        connected: Joi.boolean(),
        access_token: Joi.string().allow('', null),
        username: Joi.string().allow('', null),
        bio: Joi.string().allow('', null),
        website: Joi.string().allow('', null),
        profile_picture: Joi.string().allow('', null),
        full_name: Joi.string().allow('', null),
        id: Joi.string().allow('', null)
      })
    }
  },
  handler: function(request, reply) {
    User.findById(request.params.id, function(err, user) {
      if (err) {
        request.log(err);
        return reply(Boom.badImplementation());
      }

      if (!user) {
        return reply(Boom.unauthorized());
      }

      user = request.payload;

      return user.save(function(err, user) {
        if (err) {
          request.log('error', err);
          return reply(Boom.badData());
        }

        return reply(user.toObject());
      });
    });
  }
};

exports.password = {
  auth: { mode: 'required' },
  validate: {
    payload: {
      password: Joi.string().required(),
      newPassword: Joi.string().required()
    }
  },
  handler: function(request, reply) {
    User.findById(request.params.id, function(err, user) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      if (!user) {
        return reply(Boom.unauthorized());
      }

      user.authenticate(request.payload.password, function(err, valid) {
        if (err) {
          request.log('error', err);
          return reply(Boom.badImplementation());
        }

        if (!valid) {
          return reply(Boom.unauthorized());
        }

        user.password = request.payload.newPassword;

        user.save(function(err, user) {
          if (err) {
            request.log('error', err);
            return reply(Boom.badImplementation());
          }

          return reply(user.toObject());
        });
      });
    });
  }
};

exports.destroy = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    User.findByIdAndRemove(request.params.id, function(err) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply({ status: 'ok' });
    });
  }
};
