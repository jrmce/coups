var Joi = require('joi');
var Boom = require('boom');
var Hoek = require('hoek');
var Zip = require('../models/zip');

exports.index = {
  auth: false,
  validate: {
    query: {
      code: Joi.string().length(5),
      name: Joi.string()
    }
  },
  handler: function(request, reply) {
    Zip.find(request.query).lean().exec(function(err, zips) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(zips);
    });
  }
};

exports.find = {
  auth: {
    mode: 'required',
    scope: 'admin'
  },
  handler: function(request, reply) {
    Zip.findById(request.params.id).lean().exec(function(err, zip) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply(zip);
    });
  }
};

exports.create = {
  auth: { mode: 'required', scope: 'admin' },
  validate: {
    payload: {
      code: Joi.string().length(5).required(),
      name: Joi.string().required()
    }
  },
  handler: function(request, reply) {
    var zip = new Zip(request.payload);

    zip.save(function(err, res) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badData());
      }

      return reply(zip.toObject());
    });
  }
};

exports.update = {
  auth: { mode: 'required', scope: 'admin' },
  validate: {
    payload: {
      code: Joi.string().length(5),
      name: Joi.string()
    }
  },
  handler: function(request, reply) {
    Zip.findById(request.params.id, function(err, zip) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      if (!zip) {
        return reply(Boom.notFound());
      }

      var zip = request.payload;

      zip.save(function(err, zip) {
        if (err) {
          request.log('error', err);
          return reply(Boom.badData());
        }

        return reply(zip.toObject());
      });
    });
  }
};

exports.destroy = {
  auth: { mode: 'required', scope: 'admin' },
  handler: function(request, reply) {
    Zip.findByIdAndRemove(request.params.id, function(err) {
      if (err) {
        request.log('error', err);
        return reply(Boom.badImplementation());
      }

      return reply({ status: 'ok' });
    });
  }
};
