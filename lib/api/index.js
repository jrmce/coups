var Mongoose = require('mongoose');

var Routes = require('./routes');
var User = require('./models/user');
var Session = require('./models/session');

exports.register = function(server, options, next) {
  server.dependency(['vision', 'inert']);

  server.bind({
    config: server.settings.app.config
  });

  server.register(require('lout'), function(err) {
    if (err) {
      throw err;
    }

    server.route(Routes.endpoints);

    Mongoose.connect(server.settings.app.config.mongo, function(err) {
      if (err) {
        throw err;
      }

      return next();
    });
  });
};

validate = function(token, cb) {

};

exports.register.attributes = {
  name: 'coups-api',
  version: '1.0.0'
};
