module.exports = function(schema) {
  schema.add({ createdAt: Date, updatedAt: Date });

  return schema.pre('save', function(next) {
    var now = new Date;

    this.updatedAt = now;

    if (!this.createdAt) {
      this.createdAt = now;
    }

    return next();
  });
};
