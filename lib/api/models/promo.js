var Moment = require('moment');
var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var Timestamps = require('./plugins/timestamps');

durationValidator = function(v) {
  return /^(1|3|6|12|24)$/.test(v);
};

PromoSchema = new Schema({
  creator: { type: Schema.ObjectId, ref: 'User', required: true },
  duration: { type: Number, required: true, validate: { validator: durationValidator, message: '{VALUE} is not a valid duration.' } },
  zips: [ { type: String, maxlength: 5 } ],
  text: { type: String },
  flags: [ { type: Schema.ObjectId, ref: 'User' } ],
  saves: [ { type: Schema.ObjectId, ref: 'User' } ],
  expiresAt: { type: Date },
  instagram: {
    user: { type: Schema.Types.Mixed, required: true },
    images: { type: Schema.Types.Mixed, required: true },
    link: { type: String, required: true },
    id: { type: String, required: true },
    caption: { type: String }
  }
});

PromoSchema.pre('save', function(next) {
  this.expiresAt = Moment().add(this.duration, 'hours').toDate();

  return next();
});

PromoSchema.statics.findByZip = function(zip, cb) {
  return this.find({ zips: { $in: zip } }, cb);
};

PromoSchema.plugin(Timestamps);

module.exports = Mongoose.model('Promo', PromoSchema);
