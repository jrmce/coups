var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var Timestamps = require('./plugins/timestamps');

SessionSchema = new Schema({
  user: { type: Schema.ObjectId, ref: 'User', required: true }
});

SessionSchema.plugin(Timestamps);

module.exports = Mongoose.model('Session', SessionSchema);
