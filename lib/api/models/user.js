var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var Bcrypt = require('bcrypt');
var _ = require('underscore');
var Async = require('async');
var Promo = require('./promo');
var Timestamps = require('./plugins/timestamps');

UserSchema = new Schema({
  email: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true },
  scope: { type: String, 'enum': ['admin', 'business', 'user'], 'default': 'user' },
  saves: [ { promo: { type: Schema.ObjectId, ref: 'Promo' } } ],
  flags: [ { promo: { type: Schema.ObjectId, ref: 'Promo' } } ],
  instagram: {
    connected: { type: Boolean, 'default': false },
    access_token: { type: String },
    username: { type: String, index: { unique: true } },
    bio: { type: String },
    website: { type: String },
    profile_picture: { type: String },
    full_name: { type: String },
    id: { type: String }
  }
});

UserSchema.pre('save', function(next) {
  var calls = [];
  var self = this;

  if (this.isModified('password')) {
    calls.push(function(cb) {
      return Bcrypt.hash(self.password, 10, function(err, hash) {
        if (err) {
          return cb(err);
        }

        self.password = hash;
        return cb(null, true);
      });
    });
  }

  if (this.isModified('saves')) {
    calls.push(function(cb) {
      var id = _.last(self.saves);

      return Promo.findOne(id, function(err, promo) {
        if (err) {
          return cb(err);
        }

        promo.saves.push(self._id);

        return promo.save(function(err, res) {
          if (err) {
            return cb(err);
          }

          return cb(null, true);
        });
      });
    });
  }

  if (this.isModified('flags')) {
    calls.push(function(cb) {
      var id = _.last(self.flags);

      return Promo.findOne(id, function(err, promo) {
        if (err) {
          return cb(err);
        }

        promo.flags.push(self._id);

        return promo.save(function(err, res) {
          if (err) {
            return cb(err);
          }

          return cb(null, true);
        });
      });
    });
  }

  return Async.parallel(calls, function(err, res) {
    return next();
  });
});

UserSchema.methods.authenticate = function(candidate, cb) {
  return Bcrypt.compare(candidate, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    
    return cb(null, isMatch);
  });
};

UserSchema.options.toObject = {};

UserSchema.options.toObject.transform = function(doc, ret, options) {
  delete ret.password;
  return ret;
};

UserSchema.plugin(Timestamps);

module.exports = Mongoose.model('User', UserSchema);
