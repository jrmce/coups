var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var Timestamps = require('./plugins/timestamps');

ZipSchema = new Schema({
  code: { type: Number, index: { unique: true, required: true } },
  name: { type: String, required: true },
  imageUrl: { type: String }
});

ZipSchema.plugin(Timestamps);

module.exports = Mongoose.model('Zip', ZipSchema);
