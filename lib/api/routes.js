var Session = require('./handlers/session');
var Users = require('./handlers/users');
var Zips = require('./handlers/zips');
var Promos = require('./handlers/promos');
var Invites = require('./handlers/invites');

exports.endpoints = [
  // Session
  { method: 'POST', path: '/api/session', config: Session.create },
  { method: 'DELETE', path: '/api/session', config: Session.destroy },
  { method: 'GET', path: '/api/session/{id}', config: Session.find },

  // Users
  { method: 'GET', path: '/api/users', config: Users.index },
  { method: 'POST', path: '/api/users', config: Users.create },
  { method: 'GET', path: '/api/users/{id}', config: Users.find },
  { method: 'PATCH', path: '/api/users/{id}', config: Users.update },
  { method: 'DELETE', path: '/api/users/{id}', config: Users.destroy },
  { method: 'PUT', path: '/api/users/{id}/password', config: Users.password },

  // Zips
  { method: 'GET', path: '/api/zips', config: Zips.index },
  { method: 'POST', path: '/api/zips', config: Zips.create },
  { method: 'GET', path: '/api/zips/{id}', config: Zips.find },
  { method: 'PATCH', path: '/api/zips/{id}', config: Zips.update },
  { method: 'DELETE', path: '/api/zips/{id}', config: Zips.destroy },

  // Promos
  { method: 'GET', path: '/api/promos', config: Promos.index },
  { method: 'POST', path: '/api/promos', config: Promos.create },
  { method: 'GET', path: '/api/promos/{id}', config: Promos.find },
  { method: 'PATCH', path: '/api/promos/{id}', config: Promos.update },
  { method: 'DELETE', path: '/api/promos/{id}', config: Promos.destroy },
  { method: 'GET', path: '/api/promos/active', config: Promos.active },

  // Invites
  { method: 'GET', path: '/api/invites', config: Invites.index },
  { method: 'POST', path: '/api/invites', config: Invites.create },
  { method: 'GET', path: '/api/invites/{id}', config: Invites.find },
  { method: 'PATCH', path: '/api/invites/{id}', config: Invites.update },
  { method: 'DELETE', path: '/api/invites/{id}', config: Invites.destroy }
];
