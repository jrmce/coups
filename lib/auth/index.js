exports.register = function(server, options, next) {  
  server.auth.strategy('session', 'cookie', 'try', {
    password: options.secretKey,
    isSecure: 'false',
    redirectTo: '/login',
    redirectOnTry: 'false'
  });

  next();
};

exports.register.attributes = {
  name: 'auth'
};
