module.exports = function(env) {
  if (env === 'dev') {
    return {
      siteUrl: 'http://www.coups.com:8000',
      apiUrl: 'http://coups.com:8001/api',
      fbAppId: '880581248692114',
      fbAppSecret: '30aa632477a537c90397a92e6be4698e',
      cdn: 'https://dhdgt34w4eukj.cloudfront.net/',
      gaId: 'UA-67693504-2',
      mongo: 'mongodb://localhost/coups',
      algoliaId: 'L381R8V657',
      algoliaKey: '0ee51b6632388169353b874e0a4eee5e',
      stripeClientId: 'ca_71urkW7zWdSZWiiUfHKDY1K2Wz8DmAvy',
      stripeClientSecret: 'sk_test_jnhdcc9WyoaqgZHiXxDcMfLc',
      stripePublishableKey: 'pk_test_NXwwspkLcxWf9OD8WFFlYfUh',
      secretKey: process.env.SECRET_KEY,
      instagram: {
        instagramUrl: 'https://api.instagram.com/',
        instagramApi: 'https://api.instagram.com/v1/',
        instagramClientId: '76533cc0292c47f990844baacd540ac4',
        instagramClientSecret: 'dc2fdd5048b0448eb8138a93a3772d94',
        instagramRedirectUri: 'http://localhost:8000/link'
      }
    };
  } else if (env === 'production') {
    return {
      siteUrl: 'https://www.coups.com:8000',
      apiUrl: 'https://api.coups.com:8001',
      fbAppId: '880581248692114',
      fbAppSecret: '30aa632477a537c90397a92e6be4698e',
      cdn: 'https://dhdgt34w4eukj.cloudfront.net/',
      gaId: 'UA-67693504-1',
      mongo: process.env.MONGOLAB_URI,
      algoliaId: 'L381R8V657',
      algoliaKey: '0ee51b6632388169353b874e0a4eee5e',
      stripeClientId: 'ca_71urfsWlAPUY17CwTLMWtG1psctaYhwM',
      stripeClientSecret: 'sk_live_iySkzaHuaX18dH0c8idR35sx',
      stripePublishableKey: 'pk_live_OL6a9NjlqzjULW6U6Ki4MERY',
      secretKey: process.env.SECRET_KEY,
      instagram: {
        instagramUrl: 'https://api.instagram.com/',
        instagramApi: 'https://api.instagram.com/v1/',
        instagramClientId: '76533cc0292c47f990844baacd540ac4',
        instagramClientSecret: 'dc2fdd5048b0448eb8138a93a3772d94',
        instagramRedirectUri: 'http://localhost:8000/link'
      }
    };
  }
};
