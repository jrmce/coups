exports.check = {
  handler: function(request, reply) {
    if (!request.params.p.match(/^\d{5}$/)) {
      return reply.view('error');
    }

    var self = this;

    this.api.call({
      path: '/zips',
      query: { code: request.params.p }
    }, function(err, res) {
      if (err) {
        request.log('error', err);
        request.session.flash('notice', 'Error finding ' + request.params.p + ' promos.');
        return reply.view('error');
      }

      if (!res.body || !res.body.length) {
        request.session.flash('danger', request.params.p + ' isn\'t an active zip code.');
        return reply.view('error');
      }

      var zip = res.body[0];

      self.api.call({
        path: '/promos/active',
        query: { zips: zip.code }
      }, function(err, res) {
        if (err) {
          request.log('error', err);
          request.session.flash('notice', 'Error finding ' + request.params.zip + ' promos.');
          return reply.redirect('/');
        }

        if (!res.body || !res.body.length) {
          return reply.view('feeds/empty', zip);
        }

        var context = { promos: res.body };
        return reply.view('feeds/index', context);
      });
    });
  }
};
