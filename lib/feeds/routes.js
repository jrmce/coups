var Feeds = require('./feeds');

exports.endpoints = [
  { method: 'GET', path: '/{p*}', config: Feeds.check }
];
