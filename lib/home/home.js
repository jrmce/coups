var Zip = require('../models/zip');

exports.get = {
  handler: function(request, reply) {
    Zip.find({}, function (err, zips) {
      if (err) {
        request.log('error', err);
        request.session.flash('danger', 'Error finding zips. Please try again');
        return reply.redirect('/');
      }

      var context = { zips: zips };
      return reply.view('home', context);
    });
  }
};
