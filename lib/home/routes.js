var Home = require('./home');

exports.endpoints = [
  { method: 'GET', path: '/', config: Home.get }
];
