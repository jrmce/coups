var Instagram = require('./instagram');

exports.register = function(server, options, next) {
  var instagram = new Instagram(options.config);
  server.bind({ insta: instagram });
  next();
};

exports.register.attributes = {
  name: 'instagram'
};
