var Request = require('superagent');
var _ = require('underscore');

var Instagram = function (options) {
  this.settings = options;
}

Instagram.prototype.call = function(options, cb) {
  var method = options.method || 'GET';
  var path = this.settings.instagramApi + options.path;
  var data = options.data || {};
  var params = options.params || {};
  var query = _.extendOwn(params, {
    access_token: options.token,
    client_id: this.settings.instagramClientId
  });

  if (method === 'GET') {
    return Request.get(path).query(query).end(cb);
  }

  if (method === 'POST') {
    return Request.post(path).type('form').send(data).end(cb);
  }
};

Instagram.prototype.authorize = function(code, cb) {
  var path = this.settings.instagramUrl + "oauth/access_token";
  var data = {
    client_id: this.settings.instagramClientId,
    client_secret: this.settings.instagramClientSecret,
    grant_type: 'authorization_code',
    redirect_uri: this.settings.instagramRedirectUri,
    code: code
  };

  return Request.post(path).type('form').send(data).end(cb);
};

module.exports = Instagram;
