exports.get = {
  auth: { mode: 'required', scope: 'business' },
  handler: function(request, reply) {
    var user = request.auth.credentials.user;

    if (!user.instagram.connected) {
      request.session.flash('danger', 'You haven\'t connected your instagram account yet.');
      return reply.redirect('/settings');
    }

    this.insta.call({
      path: 'users/self/media/recent',
      token: user.instagram.access_token
    }, function(err, res) {
      if (err) {
        request.log('error', err);
        request.session.flash('danger', 'There was an error getting your instagram photos. Please try again.');
        return reply.redirect('/');
      }

      var pagination = res.body.pagination;
      var context = { photos: res.body.data };
      return reply.view('manage/show', context);
    });
  }
};
