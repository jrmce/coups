var Manage = require('./manage');

exports.endpoints = [
  { method: 'GET', path: '/manage', config: Manage.get }
];
