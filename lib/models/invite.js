var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var Shortid = require('shortid');
var Timestamps = require('./plugins/timestamps');

InviteSchema = new Schema({
  code: { type: String, required: true, "default": Shortid.generate, index: { unique: true } },
  active: { type: Boolean, required: true, "default": false }
});

InviteSchema.plugin(Timestamps);

module.exports = Mongoose.model('Invite', InviteSchema);
