var Async = require('async');
var Moment = require('moment');
var _ = require('underscore');

exports.new = {
  auth: { mode: 'required', scope: 'business' },
  handler: function(request, reply) {
    if (!request.query.iid) {
      request.session.flash('danger', 'You must select an image before creating a promo.');
      return reply.redirect('/manage');
    }

    var self = this;

    this.api.call({ path: '/zips' }, function(err, res) {
      if (err) {
        request.log('error', err);
        request.session.flash('danger', 'There was an error with your request. Please try again.');
        return reply.redirect('/manage');
      }

      var zips = res.body;
      var user = request.auth.credentials.user;

      self.insta.call({
        path: 'media/' + request.query.iid,
        token: user.instagram.access_token
      }, function(err, res) {
        if (err) {
          request.log('error', err);
          request.session.flash('danger', 'There was an error with your request. Please try again.');
          return reply(err);
        }

        var context = { photo: res.body.data, zips: zips };
        return reply.view('promos/new', context);
      });
    });
  }
};

exports.create = {
  auth: { mode: 'required', scope: 'business' },
  handler: function(request, reply) {
    var user = request.auth.credentials.user;
    var promo = _.omit(request.payload, 'iid');
    promo.creator = user._id;

    this.insta.call({
      path: 'media/' + request.payload.iid,
      token: user.instagram.access_token
    }, function(err, res) {
      promo.instagram = _.pick(res.body.data, 'images', 'user', 'link', 'id');
      promo.instagram.caption = res.body.data.caption ? res.body.data.caption.text : '';

      self.api.call({
        method: 'POST',
        path: '/promos',
        data: promo
      }, function(err, res) {
        if (err) {
          request.log('error', err);
          request.session.flash('danger', 'There was an error creating your promo. Please try again.');
          return reply.redirect('/promos/new?iid=' + promo.instagram.id);
        }

        request.session.flash('info', 'Promo successfully created.');
        return reply.redirect('/promos/user/' + user.instagram.username);
      });
    });
  }
};

exports.show = {
  handler: function(request, reply) {
    this.api.call({
      path: '/promos/' + request.params.id
    }, function(err, res) {
      if (err) {
        request.log('error', err);
        request.session.flash('danger', 'There was an error getting the promo. Please try again.');
        return reply.redirect('/');
      }

      return reply.view('promos/show', res.body);
    });
  }
};

exports.user = {
  handler: function(request, reply) {
    this.api.call({
      path: '/promos',
      query: {
        username: request.params.username
      }
    }, function(err, res) {
      if (err) {
        request.log('error', err);
        request.session.flash('danger', 'There was an error getting the user\'s promos. Please try again.');
        return reply.redirect('/');
      }

      if (!res.body || !res.body.length) {
        return reply.view('promos/empty', {
          username: request.params.username
        });
      }

      var context = { promos: res.body };
      return reply.view('promos/user', context);
    });
  }
};
