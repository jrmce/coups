var Promos = require('./promos');

exports.endpoints = [
  { method: 'GET', path: '/promos/new', config: Promos.new },
  { method: 'POST', path: '/promos', config: Promos.create },
  { method: 'GET', path: '/promos/{id}', config: Promos.show },
  { method: 'GET', path: '/promos/user/{username}', config: Promos.user }
];
