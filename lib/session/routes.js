var Session = require('./session');

exports.endpoints = [
  { method: 'GET', path: '/login', config: Session.new },
  { method: 'POST', path: '/login', config: Session.login },
  { method: 'GET', path: '/logout', config: Session.logout }
];
