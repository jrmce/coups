var User = require('../models/user');

exports.new = {
  handler: function(request, reply) {
    var context = { email: request.session.get('login') };
    return reply.view('session/login', context);
  }
};

exports.login = {
  handler: function(request, reply) {
    User.findOne({ email: request.payload.email }, function (err, user) {
      if (err ||
          !user) {
        request.session.flash('danger', 'Email or password is invalid. Please try again.');
        request.session.set('login', request.payload.email);
        return reply.redirect('/login');
      }

      user.authenticate(request.payload.password, function (err, valid) {
        if (err ||
            !valid) {
          request.session.flash('danger', 'Email or password is invalid. Please try again.');
          request.session.set('login', request.payload.email);
          return reply.redirect('/login');
        }

        request.session.clear('login');
        request.session.flash('info', 'Welcome back.');
        request.auth.session.set(user);
        return reply.redirect('/');
      });
    });
  }
};

exports.logout = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    request.auth.session.clear();
    request.session.flash('info', 'You\'ve been logged out.');
    return reply.redirect('/');
  }
};
