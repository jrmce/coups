var Hoek = require('hoek');
var Mongoose = require('mongoose');
var Hoek = require('hoek');

var internals = {};

exports.register = function(server, options, next) {
  server.ext('onPreResponse', internals.onPreResponse);

  var config = options.config
  server.bind({ config: config });

  server.register({
    register: require('yar'),
    options: {
      cookieOptions: {
        password: config.secretKey,
        isSecure: false
      }
    }
  }, function(err) {
    Hoek.assert(!err, err);

    Mongoose.connect(config.mongo, function(err) {
      Hoek.assert(!err, err);
      return next();
    });
  });
};

exports.register.attributes = {
  name: 'setup'
};

internals.onPreResponse = function(request, reply) {
  var response = request.response;

  if (response.variety === 'view') {
    if (!response.source.context) {
      response.source.context = {};
    }

    var context = response.source.context;
    var warning = request.session.flash('warning');
    var danger = request.session.flash('danger');
    var info = request.session.flash('info');
    var success = request.session.flash('success');

    context.path = request.path;
    context.config = this.config;
    context.flash = {};

    if (info.length) {
      context.flash.info = info;
    }

    if (danger.length) {
      context.flash.danger = danger;
    }

    if (warning.length) {
      context.flash.warning = warning;
    }

    if (success.length) {
      context.flash.success = success;
    }

    if (request.auth.isAuthenticated) {
      context.user = request.auth.credentials;
      context.scope = request.auth.credentials.scope;
      context.navPartial = context.scope + 'Nav';
    }

    return reply.continue();
  }
  return reply.continue();
};
