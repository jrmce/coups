var Hoek = require('hoek');

var Server = require('./index');
var Config = require('./config');

var internals = {};

internals.config = Config(process.env.NODE_ENV);

internals.manifest = {
  connections: [{ port: 8000 }],
  plugins: [{
      'good': [{
          options: {
            reporters: [
              {
                reporter: require('good-console'),
                events: { log: '*', response: '*', ops: '*', error: '*', request: '*' }
              }
            ]
          }
        }]},
    { 'crumb': null },
    { 'inert': null },
    { 'vision': null },
    { 'visionary': {
        engines: { hbs: 'handlebars' },
        relativeTo: __dirname,
        path: 'templates',
        layoutPath: 'templates/layout',
        layout: true,
        helpersPath: 'templates/helpers',
        partialsPath: 'templates/partials'
      }
    },
    { 'hapi-auth-cookie': null },
    { './static': null },
    { './setup': { config: internals.config } },
    { './auth': { secretKey: internals.config.secretKey } },
    { './instagram': { config: internals.config.instagram } },
    { './home': null },
    { './feeds': null },
    { './manage': null },
    { './users': null },
    { './promos': null },
    { './session': null },
  ]
};

internals.options = { relativeTo: __dirname };

Server.init(internals.manifest, internals.options, function (err, server) {
  Hoek.assert(!err, err);
  console.log('Server started at: ' + server.info.uri);
})
