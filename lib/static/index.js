var Path = require('path');

exports.register = function(server, options, next) {
  server.route({
    method: 'GET',
    path: '/public/{file*}',
    config: { auth: false },
    handler: {
      directory: {
        path: Path.resolve(__dirname, '../../public')
      }
    }
  });

  next();
};

exports.register.attributes = {
  name: 'static'
};
