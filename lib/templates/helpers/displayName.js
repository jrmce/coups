module.exports = function(user) {
  if (user.instagram) {
    if (user.instagram.full_name) {
      return user.instagram.full_name;
    }
  }
  
  return user.email;
};
