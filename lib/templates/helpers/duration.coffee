Moment = require 'moment'

module.exports = (date) ->
  if Moment().isAfter date then return 'Expired'

  expiresAt = Moment().diff date, 'hours'

  if expiresAt
    return "#{Math.abs expiresAt}h"
  else
    expiresAt = Moment().diff date, 'minutes'
    return "#{Math.abs expiresAt}m"
