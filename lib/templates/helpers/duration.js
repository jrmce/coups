var Moment = require('moment');

module.exports = function(date) {
  if (Moment().isAfter(date)) {
    return 'Expired!';
  }
  
  return 'Expires in ' + Moment().to(date, true);
};
