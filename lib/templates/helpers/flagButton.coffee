_ = require 'underscore'
Handlebars = require 'handlebars'

module.exports = ->
  contains = (flags, id) ->
    return _.find flags, (flag) ->
      if flag._id is id then return yes

  if contains @user.flags, @promo._id
    return new Handlebars.SafeString """
      <a class="btn btn-danger btn-sm js-flag-promo disabled" href="javascript:void(0)">Flagged</a>
    """

  return new Handlebars.SafeString """
    <a class="btn btn-danger btn-sm js-flag-promo" href="javascript:void(0)">Flag</a>
  """
