module.exports = function(options) {
  if (this.scope === 'admin') {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
};
