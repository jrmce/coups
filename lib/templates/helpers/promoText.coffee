Handlebars = require 'handlebars'

module.exports = (promo) ->
  text = if promo.text then promo.text else promo.instagram.caption
  if text then return new Handlebars.SafeString "<p>#{text}</p>"
