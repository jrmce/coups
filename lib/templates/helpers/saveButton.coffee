_ = require 'underscore'
Handlebars = require 'handlebars'

module.exports = ->
  contains = (saves, id) ->
    return _.find saves, (save) ->
      if save._id is id then return yes

  if contains @user.saves, @promo._id
    return new Handlebars.SafeString '<a class="btn btn-success btn-sm js-save-promo disabled" href="javascript:void(0)">Saved</a>'

  return new Handlebars.SafeString '<a class="btn btn-success btn-sm js-save-promo" href="javascript:void(0)">Save</a>'
