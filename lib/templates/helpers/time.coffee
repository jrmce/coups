Moment = require 'moment'

module.exports = (time) ->
  return Moment(time).format('l LT')
