var Users = require('./users');

exports.endpoints = [
  { method: 'GET',  path: '/register',        config: Users.newUser },
  { method: 'POST', path: '/register',        config: Users.createUser },
  { method: 'GET',  path: '/business',        config: Users.newBusiness },
  { method: 'POST', path: '/business',        config: Users.createBusiness },
  { method: 'GET',  path: '/settings',        config: Users.settings },
  { method: 'POST', path: '/settings',        config: Users.update },
  { method: 'POST', path: '/update-password', config: Users.updatePassword },
  { method: 'GET',  path: '/link',            config: Users.link },
  { method: 'GET',  path: '/unlink',          config: Users.unlink }
];
