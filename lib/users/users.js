var Request = require('superagent');
var Hoek = require('hoek');

var User = require('../models/user');
var Invite = require('../models/invite');

var internals = {};

internals.passwordCheck = function(request, reply) {
  if (request.payload.password !== request.payload.passwordConfirmation) {
    request.session.flash('danger', 'Passwords don\'t match.');
    request.session.set('register', request.payload.email);
    return reply.redirect(request.info.referrer).takeover();
  }

  return reply(true);
};

internals.emailCheck = function(request, reply) {
  User.findOne({ email: request.payload.email }, function(err, user) {
    if (err) {
      request.session.flash('danger', 'There was an error. Please try again.');
      request.session.set('register', request.payload.email);
      return reply.redirect(request.info.referrer).takeover();
    }

    if (user) {
      request.session.flash('danger', 'Email already in use.');
      request.session.set('register', request.payload.email);
      return reply.redirect(request.info.referrer).takeover();
    }

    return reply(true);
  });
};

internals.inviteCheck = function(request, reply) {
  if (!request.payload.invite) {
    request.session.flash('danger', 'Most provide an invite code.');
    request.session.set('register', request.payload.email);
    return reply.redirect(request.info.referrer).takeover();
  }

  Invite.findOne({ code: request.payload.invite }, function(err, invite) {
    if (err ||
        !invite) {
      request.session.flash('danger', 'Invalid invite code. Please try again');
      request.session.set('register', request.payload.email);
      return reply.redirect(request.info.referrer).takeover();
    }

    return reply(invite);
  });
};

exports.newUser = {
  handler: function(request, reply) {
    var context = { email: request.session.get('register') };
    return reply.view('users/register', context);
  }
};

exports.newBusiness = {
  handler: function(request, reply) {
    var context = { email: request.session.get('register') };
    return reply.view('users/business', context);
  }
};

exports.createUser = {
  pre: [
    [
      { method: internals.passwordCheck, assign: 'passwordCheck' },
      { method: internals.emailCheck, assign: 'emailCheck' }
    ]
  ],
  handler: function(request, reply) {
    var user = new User({
      email: request.payload.email,
      password: request.payload.password
    });

    user.save(function(err, user) {
      if (err) {
        request.session.flash('danger', 'There was an unknown error. Please try again.');
        request.session.set('register', request.payload.email);
        return reply.redirect('/register');
      }

      request.session.clear('register');
      request.auth.session.set(user);
      request.session.flash('info', 'Thanks for signing up.');
      return reply.redirect('/')
    });
  }
};

exports.createBusiness = {
  pre: [
    [
      { method: internals.passwordCheck, assign: 'passwordCheck' },
      { method: internals.emailCheck, assign: 'emailCheck' },
      { method: internals.inviteCheck, assign: 'inviteCheck' }
    ]
  ],
  handler: function(request, reply) {
    var invite = request.pre.inviteCheck;
    invite.active = true;

    var user = new User({
      email: request.payload.email,
      password: request.payload.password,
      scope: request.payload.scope
    });

    Async.series({
      user: function(cb) {
        user.save(function(err, user) {
          if (err) {
            return cb(err);
          }

          return cb(null, user[0]);
        });
      },
      // Save invite
      invite: function(cb) {
        invite.save(cb);
      }
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'There was an error creating your account. Please try again.');
        request.session.set('register', request.payload.email);
        return reply.redirect('/business');
      }

      request.session.clear('register');
      request.auth.session.set(res.user);
      request.session.flash('info', 'Thanks for signing up.');
      return reply.redirect('/')
    });
  }
};

exports.settings = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    return reply.view('users/settings');
  }
};

exports.update = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    var user = request.auth.credentials.user;

    this.api.call({
      method: 'PATCH',
      path: '/users/' + user._id,
      data: request.payload,
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'There was an error updating your account. Please try again.');
        return reply.redirect('/settings');
      }

      request.auth.session.set('user', res.body);
      request.session.flash('info', 'You successfully updated your account.');
      return reply.redirect('/settings');
    });
  }
};

exports.updatePassword = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    if (request.payload.newPassword !== request.payload.newPasswordConfirmation) {
      request.session.flash('danger', 'Passwords don\'t match');
      return reply.redirect('/settings');
    }

    var user = request.auth.credentials.user;

    this.api.call({
      method: 'PUT',
      path: '/users/' + user._id + '/password',
      data: {
        password: request.payload.password,
        newPassword: request.payload.newPassword
      }
    }, function(err, res) {
      if (err) {
        request.session.flash('danger', 'There was an error updating your password. Please try again.');
        return reply.redirect('/settings');
      }

      request.session.flash('info', 'You successfully updated your password.');
      return reply.redirect('/settings');
    });
  }
};

exports.unlink = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    var user = request.auth.credentials.user;
    var data = {
      instagram: {
        connected: false,
        access_token: null,
        username: null,
        id: null,
        website: null,
        bio: null,
        profile_picture: null,
        full_name: null
      }
    };

    this.api.call({
      method: 'PATCH',
      path: '/users/' + user._id,
      data: data
    }, function(err, res) {
      if (err) {
        request.log('error', err);
        request.session.flash('danger', 'There was an error unlinking your account with Instagram. Please try again.');
        return reply.redirect('/settings');
      }

      request.auth.session.set('user', res.body);
      request.session.flash('info', 'You successfully unlinked your account with Instagram.');
      return reply.redirect('/settings');
    });
  }
};

exports.link = {
  auth: { mode: 'required' },
  handler: function(request, reply) {
    if (!request.query.code) {
      request.session.flash('danger', 'There was an error linking your account with Instagram. Please try again.');
      return reply.redirect('/settings');
    }

    var self = this;

    this.insta.authorize(request.query.code, function(err, res) {
      if (err) {
        request.log('error', err);
        request.session.flash('danger', 'There was an error linking your account with Instagram. Please try again.');
        return reply.redirect('/settings');
      }

      var user = request.auth.credentials.user;
      var instagram = {
        connected: true,
        access_token: res.body.access_token
      };

      var instagram = Hoek.merge(instagram, res.body.user);
      var data = { instagram: instagram };

      console.log(request.state.sid);
      self.api.call({
        method: 'PATCH',
        path: '/users/' + user._id,
        data: data,
        headers: { 'Cookie': request.state.sid }
      }, function(err, res) {
        if (err) {
          request.log('error', err);
          request.session.flash('danger', 'There was an error linking your account with Instagram. Please try again.');
          return reply.redirect('/settings');
        }

        request.auth.session.set('user', res.body);
        request.session.flash('info', 'You successfully linked your account with Instagram!');
        return reply.redirect('/settings');
      });
    });
  }
};
