module.exports = (env) ->
  if env is 'dev'
    siteUrl: 'http://www.coups.com:8000'
    apiUrl: 'http://api.coups.com:8001'
    cdn: 'https://dhdgt34w4eukj.cloudfront.net/'
  else if env is 'production'
    siteUrl: 'https://www.coups.com:8000'
    apiUrl: 'https://api.coups.com:8001'
    cdn: 'https://dhdgt34w4eukj.cloudfront.net/'
