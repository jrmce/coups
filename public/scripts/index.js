var $ = global.jQuery = require('jquery');
require('bootstrap');

var LoadingButtons = require('./modules/loading-button');
var DisabledLinks = require('./modules/disabled-link');

$(document).ready(function() {
  LoadingButtons()
  DisabledLinks()
});
