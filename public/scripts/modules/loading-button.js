var $ = require('jquery');

module.exports = function () {
  $('.js-show-loader').on('click', function (e) {
    $(this).find('i.fa').removeClass('hidden');
  });
};
