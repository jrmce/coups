var Lab = require('lab');
var Code = require('code');
var Hapi = require('hapi');
var Path = require('path');

var Knedge = require('../lib');

var internals = {};

var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;

describe('Init', function() {
	it('starts server and returns hapi server object', function(done) {
		Knedge.init({}, {}, function(err, server) {
			expect(err).to.not.exist();
      expect(server).to.be.instanceof(Hapi.Server);
      server.stop(done);
		});
	});

	it('starts server on provided port', function (done) {
    Knedge.init({ connections: [{ port: 5000 }] }, {}, function(err, server) {
      expect(err).to.not.exist();
      expect(server.info.port).to.equal(5000);
      server.stop(done);
    });
	});

	it('handles register plugin errors', { parallel: false }, function(done) {
		var manifest = {
			connections: [{ port: 0 }],
			plugins: {
				'./plugin': null
			}
		};

		var options = {
			relativeTo: __dirname
		};

    Knedge.init(manifest, options, function (err, server) {
        expect(err).to.exist();
        expect(err.message).to.equal('registration failed!');
        done();
    });
	});
});
